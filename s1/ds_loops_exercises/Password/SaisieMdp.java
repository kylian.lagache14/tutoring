/**
 * Program that checks if a String contains at least 8 characters
 * and if it starts with a capital letter.
 */
public class SaisieMdp extends Program {
    /**
     * Using a function to avoid calling multiple times charAt(sentence,0)
     */
    boolean isCapitalLetter(char c) {
        return 'A' <= c && c <= 'Z';
    }
    void algorithm() {
        String sentence;
        do {
            sentence = readString();
        } while(length(sentence) >= 8 && isCapitalLetter(charAt(sentence, 0)));
        println("The password is okay. ;)");
    }
}