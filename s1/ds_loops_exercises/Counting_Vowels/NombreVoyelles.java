/**
 * The base version for the "NombreVoyelles" program.
 */
public class NombreVoyelles extends Program {
    void algorithm() {
        String sentence = readString();
        int count = 0;
        for (int i = 0; i < length(sentence); i = i + 1) {
            char letter = charAt(sentence, i);
            if (letter == 'a' || letter == 'e' // You can put those on multiple lines
               || letter == 'i' || letter == 'o'
               || letter == 'u' || letter == 'y') {
                count = count + 1;
            }
        }
        println("You've got " + count + " vowels in this sentence !");
    }
}