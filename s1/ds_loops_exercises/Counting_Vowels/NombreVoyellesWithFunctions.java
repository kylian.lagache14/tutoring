/**
 * NombreVoyelles but abstracting the work with some functions.
 */
public class NombreVoyellesWithFunctions extends Program {

    /**
     * The isVowel predicate but simplified
     */
    boolean isVowelWithStep(char c) {
        boolean ret = false;
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y') {
            ret = true;
        }
        return ret;
    }

    /**
     * Predicate made to check if a letter is a vowel.
     */
    boolean isVowel(char c) {
        return c == 'a' || c == 'e' // You can put those on multiple lines
            || c == 'i' || c == 'o'
            || c == 'u' || c == 'y';
    }

    int incIfVowel(char c) {
        int returnValue = 0;
        if (isVowel(c)) {
            returnValue = 1;
        }
        return returnValue;
    }

    void algorithm() {
        String sentence = readString();
        int count = 0;
        for (int i = 0; i < length(sentence); i = i + 1) {
            char letter = charAt(sentence, i);
            count = count + incIfVowel(letter);
        }
        println("You've got " + count + " vowels in this sentence !");
    }
}