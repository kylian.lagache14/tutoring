/**
 * Trying to find out if a sentence contains a question mark ('?').
 */
public class Question extends Program {
  void algorithm() {
    String sentence = readString();
    int counter = 0;
    boolean found = false;
    while (!found && counter < length(sentence)) {
        if (charAt(sentence, counter) == '?') {
            found = true;
        } else {
            counter = counter + 1;
        }
    }
  }

    /**
    * For this version we are guessing the the sentence is written using a correct language like :
    * -> "What do you mean ?"
    * rather than
    * -> "? ugh"
    * So the question mark should always be near the end of the sentence if one should appear.
    */
    int optimizedVersion() {
      String sentence = readString();
      int counter = length(sentence) - 1;
      boolean found = false;
      while (!found && counter > 0) {
          if (charAt(sentence, counter) == '?') {
              found = true;
          } else {
              counter = counter - 1;
          }
      }
        return counter;
  }
}